import { useEffect, useState } from 'react';
import './App.css';
import Result from './components/Result/Result';
import SingleCard from './components/SingleCard/SingleCard';
import axios from 'axios';
import { LastScores } from './components/LastScores/LastScores';

const cardsImgs = [
  { "src": "/img/helmet-1.png", matched:false },
  { "src": "/img/potion-1.png", matched:false },
  { "src": "/img/ring-1.png",   matched:false },
  { "src": "/img/scroll-1.png", matched:false },
  { "src": "/img/shield-1.png", matched:false },
  { "src": "/img/sword-1.png",  matched:false }
]

function App() {
  const [cards, setCards] = useState([]);
  const [turns, setTurns] = useState(0);
  const [choiceOne, setChoiceOne] = useState(null);
  const [choiceTwo, setChoiceTwo] = useState(null);
  const [disabled, setDisabled] = useState(false);
  const [isFinished, setIsFinished] = useState(false);
  const [clicksPlayerOne, setClicksPlayerOne] = useState(0);
  const [clicksPlayerTwo, setClicksPlayerTwo] = useState(0);
  const [pairsPlayerOne, setPairsPlayerOne] = useState(0);
  const [pairsPlayerTwo, setPairsPlayerTwo] = useState(0);
  const [playerOne, setPlayerOne] = useState('');
  const [playerTwo, setPlayerTwo] = useState('');
  const [gameMode, setGameMode] = useState(1);
  const [submitedForm, setSubmitedForm] = useState(false);
  const [activePlayerOne, setActivePlayerOne] = useState(true);
  const [winner, setWinner] = useState('');
  const [playerId, setPlayerId] = useState(null);
  const [lastScores, setLastScores] = useState(null);
  const [showLastScores, setShowLastScores] = useState(false);

  const showScores = () =>{
    setShowLastScores(!showLastScores)
  }
  
  const getScores = async() =>{
    const data = await axios.get(`http://localhost:4000/api/player/${playerOne}`)
    setPlayerId(data.data.playerId)
    setLastScores(data.data.scores)
  }

  const sendData = async() => {
    const player = await axios.post(`http://localhost:4000/api/addPlayer`, { username:playerOne })
     setPlayerId(player.data.playerId);
  }

  const sendScore = async() => {
    const player = await axios.post(`http://localhost:4000/api/addScore`, { playerId:playerId, score:clicksPlayerOne })
  }

   //shuffle cards
  const shuffle = () =>{
    getScores()
    const shuffled = [...cardsImgs, ...cardsImgs]
     .sort(() => Math.random() - 0.5)
     .map((card) => ({ ...card, id:Math.random()}))
     
     setCards(shuffled);
     setTurns(0);
     setActivePlayerOne(true);
     setClicksPlayerOne(0);
     setClicksPlayerTwo(0);
     setPairsPlayerOne(0);
     setPairsPlayerTwo(0);
     setIsFinished(false);
     setChoiceOne(null);
     setChoiceTwo(null);  
    }

    const handleChangePlayerOne = (event) => {
      setPlayerOne(event.target.value);
    }

    const handleChangePlayerTwo= (event) => {
      setPlayerTwo(event.target.value);
    }

    const singlePlayer = () => {
      setPlayerOne('');
      setPlayerTwo('');
      setGameMode(1);
    }

    const multiPlayer = () => {
      setGameMode(2);
      setPlayerOne('');
      setPlayerTwo('');
    }

    const handleSubmit = (e) => {
       e.preventDefault();
       if(gameMode == 1 && playerOne !== '' || gameMode == 2 
          && playerOne !== '' && playerTwo !== '' ){
          setSubmitedForm(true);
          if(gameMode == 1){
            getScores();
            sendData();
          }
      }   
     };

    // handle choice
    const handleChoice = (card) => {
      //if choice one is fill then fill choiceTwo, else fill choiceOne
      choiceOne ? setChoiceTwo(card) : setChoiceOne(card);
      activePlayerOne ? setClicksPlayerOne(prevClicks => prevClicks += 1): setClicksPlayerTwo(prevClicks => prevClicks += 1)
     
 }
    useEffect(() =>{
        let checkMatchedCards= cards.filter(card => card.matched === true).length;
         if(checkMatchedCards == 12){
           if(pairsPlayerOne > pairsPlayerTwo){
               setWinner(playerOne)
           } else if (pairsPlayerOne == pairsPlayerTwo){
              setWinner('Draw')
           } else{
            setWinner(playerTwo)
           }
           setIsFinished(true); 
           sendScore();  
         }
    },[choiceTwo])

    // start game (when we load page)
    useEffect(() => {
      if(true){
      shuffle();
    }
    },[])

    //reset choices and increase turn
    const resetTurn = () => {
      setChoiceOne(null)
      setChoiceTwo(null)
      setTurns(prevTurns => prevTurns + 1)
      setDisabled(false)
    }

  
    //selected cars (compare)
    useEffect(() => {
      if(choiceOne && choiceTwo) {
        setDisabled(true);
        if(choiceOne.src === choiceTwo.src){
          activePlayerOne ? setPairsPlayerOne(prevClicks => prevClicks += 1): setPairsPlayerTwo(prevClicks => prevClicks += 1)
          setCards(prevCards => {
            return prevCards.map(card => {
              if(card.src === choiceOne.src){
                return {...card, matched: true}
               }else{
                return card
              }
            })
          })
          resetTurn();
        } else{
          if(gameMode == 2){
          setActivePlayerOne(!activePlayerOne)
          }
          setTimeout(() => resetTurn(),500) 
        }
      }
    },[choiceOne, choiceTwo])

  return (
    <div className="App">
      <h1>Memory Game</h1>
      { submitedForm  && lastScores && gameMode == 1 && <button onClick={showScores}>Last Scores</button> }
      { showLastScores && lastScores.map(score => (
        <LastScores 
        key={score.scoreId} 
        scores={score} 
        />
      ))}
      { submitedForm && <button onClick={shuffle}>New Game</button> }
      { !submitedForm && <div className='game-mode'>
        <button className= {gameMode == 1? 'activeMode' : ''} onClick = {singlePlayer}>Single-Player</button>
        <button className= {gameMode == 2? 'activeMode' : ''} onClick = {multiPlayer}>Multi-Player</button>
      </div>}
      
      {!submitedForm && 
       <form>
        <div className='mode'>
          { gameMode == 1 | gameMode == 2 && <input value={playerOne} onChange={handleChangePlayerOne} placeholder='player 1' />}
          { gameMode == 2 && <input value = {playerTwo} onChange={handleChangePlayerTwo} placeholder='player 2' />}
        </div> 
         <button onClick={handleSubmit}>Start</button>
      </form>}

      { submitedForm &&<div className='results'>
        <div className = {activePlayerOne && gameMode == 2 ? 'activePlayer' : ''}>{playerOne && <Result  clicks={clicksPlayerOne} foundPairs={pairsPlayerOne} scores= {'scored'} username = {playerOne}/>}</div>
        <div className = {!activePlayerOne && gameMode == 2 ? 'activePlayer' : ''}>{ gameMode == 2 && playerTwo && <Result  clicks={clicksPlayerTwo} foundPairs={pairsPlayerTwo} scores= {'scored'} username = {playerTwo}/>}</div>
      </div>}

      { isFinished && <h1>{winner}! Congratulations.</h1> }
      
      {submitedForm && <div className='card-grid'>
      { cards.map(card => (
        <SingleCard 
        key={card.id} 
        card={card} 
        handleChoice={handleChoice}
        flipped={card === choiceOne || card === choiceTwo || card.matched }
        />
      ))}
      </div>}
    </div>
  );
}

export default App;
