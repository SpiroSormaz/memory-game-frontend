
export function LastScores({scores}) {
   return(
       <div className="last-scores">
           <p>{scores.score}</p>
           <span>{Date(scores.date).slice(0,21)}</span>
       </div>
   )
}