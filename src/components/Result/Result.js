import './Result.css'
export default function Result({ clicks, foundPairs, scores, username }) {
    return(
        <div className="result">
            <h3>{username}</h3>
            <p>Clicks: {clicks}</p>
            <p>Found Paris: {foundPairs}</p>
        </div>
    )
}